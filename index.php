<?php
include 'php/cargarJson.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MUNDOFRANQUICIA</title>
<meta name="description" content="Activos Mundofranquicia"/>
<meta name="author" content="Diego Bartolomé">
<link rel="icon" href="./imagenes/favicon.png" sizes="32x32">
<!-- Latest compiled and minified CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

<link rel="stylesheet" href="css/estilo.css">
<script src="https://kit.fontawesome.com/f05075dfa2.js" crossorigin="anonymous"></script>


</head>

<body>
<div class="capaLoader"> <div class="preloader"></div></div>

    <section class="container bannerheader p-2">
        <img src="/imagenes/oh-my-veggie-28219-banner1260x110.gif" width="100%">
    </section>
<header class="py-4">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-6 col-md-4">
                <img src="/imagenes/logo-mundofranquiciaMesa-de-trabajo-1.png" width="100%">
            </div>
            <div class="col-6 col-md-3 text-end align-self-center">
                <div class="contRedes">
                    <a href="https://www.facebook.com/Mundofranquicia-Consulting-107778329266086/" target="_blank" class="ico-redes facebookIco"><i class="fa-brands fa-facebook fa-2x"></i></a>
                    <a href="https://www.facebook.com/Mundofranquicia-Consulting-107778329266086/" target="_blank" class="ico-redes twitterIco"><i class="fa-brands fa-twitter fa-2x"></i></a>
                    <a href="https://www.youtube.com/channel/UCG5zhkxY0ohstUmTduf6eXQ" target="_blank" class="ico-redes youtubeIco"><i class="fa-brands fa-youtube fa-2x"></i></a>
                    <a href="https://www.youtube.com/channel/UCG5zhkxY0ohstUmTduf6eXQ" target="_blank" class="ico-redes linkedinIco"><i class="fa-brands fa-linkedin fa-2x"></i></i></a>
                </div>
            </div>
        </div>
    </div> 
</header>

    <div class="capaFormulario">
        <div class="contFormulario">
            <div class="btnCerrar">X</div>
            <form action="/php/enviar_mail.php" method="post"  enctype="multipart/form-data" name="formulario" class="needs-validation">
                <div class="form-floating mb-3">
                    <input type="text" name="Nombre" class="form-control" id="floatingNombre" placeholder="Nombre" required>
                    <label for="floatingNombre form-check-label">Nombre</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" name="Telefono" class="form-control" id="floatingTelefono" placeholder="Teléfono" required>
                    <label for="floatingTelefono form-check-label">Teléfono</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="email" name="email" class="form-control" id="floatingEmail" placeholder="name@example.com" required>
                    <label for="floatingEmail form-check-label">Email</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" name="activo" class="form-control" id="floatingRef" placeholder="Ref Activo" required>
                    <label for="floatingRef form-check-label">Ref Activo</label>
                </div>
                <div class="col-12">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="loatingCheck1" required>
                        <label class="form-check-label" for="invalidCheck2">
                           Acepta la política de privacidad
                        </label>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="loatingCheck2" required>
                        <label class="form-check-label" for="invalidCheck2">
                            Acepta comunicaciones comerciales
                        </label>
                    </div>
                </div>
                <div class="col-12 text-end">
                    <button class="btn btn-dark" type="submit" style="text-transform: uppercase">Enviar Formulario</button>
                </div>
            </form>

        </div>

    </div>
<?php

?>
<div class="container mb-3">
   
    <div class="row g-2 justify-content-center">
        <?php 
          /*
            echo '<pre>';
                print_r($provLocalidad);
            echo '</pre>';
          */

            $provinciaSelect = '';
            $localidadSelect = '';
            $switchProvincia = '';
            $comparaProvincia = '';
            $switchLocalidad = '';
            $comparaLocalidad = '';
            array_multisort($provLocalidad);
      
            $tipoSelect = '';
            $subtipoSelect = '';
            $switchTipo = '';
            $comparaTipo = '';
            $switchSubtipo = '';
            $comparaSubtipo= '';
            array_multisort($tipoActivo);

               /*
                    echo '<pre>';
                        print_r($tipoActivo);
                    echo '</pre>';
               */
            for($i = 0; $i<count($provLocalidad); $i++) {
            
                $switchProvincia = $provLocalidad[$i]['provincia'];
                if($switchProvincia != $comparaProvincia) {
                    $provMinusculas = strtolower($provLocalidad[$i]['provincia']);
                    $provMinusculasSinAcentos = eliminar_acentos($provMinusculas);
                    $provinciaSelect .= '<option value="'.$provMinusculasSinAcentos.'">'.strtoupper($provLocalidad[$i]['provincia']).'</option>';
                    $comparaProvincia = $switchProvincia;
                }
                //$provLocalidad[$i]['provincia']
                $switchLocalidad  = $provLocalidad[$i]['localidad'];
                if($switchLocalidad  != $comparaLocalidad) {
                    $provMinusculas = strtolower($provLocalidad[$i]['provincia']);
                    $provMinusculasSinAcentos = eliminar_acentos($provMinusculas);
                    $locaMinusculas = strtolower($provLocalidad[$i]['localidad']);
                    $locaMinusculasSinAventos = eliminar_acentos($locaMinusculas);
                    $localidadSelect .= '<option value="'. $locaMinusculasSinAventos .'" class="'.$provMinusculasSinAcentos.'">'.$provLocalidad[$i]['localidad'].'</option>';
                    $comparaLocalidad = $switchLocalidad;
                }
            }


        for($i = 0; $i<count($tipoActivo); $i++) {

            $switchTipo = $tipoActivo[$i]['tipo'];
            if($switchTipo != $comparaTipo) {
                $tipoSelect .= '<option value="'.$tipoActivo[$i]['tipo'].'">'.strtoupper($tipoActivo[$i]['tipo']).'</option>';
                $comparaTipo = $switchTipo;
            }
            //$provLocalidad[$i]['provincia']
            $switchSubtipo  = $tipoActivo[$i]['subtipo'];
            if($switchSubtipo  != $comparaSubtipo) {
                $subtipoSelect .= '<option value="'. $tipoActivo[$i]['subtipo'] .'" class="'.$tipoActivo[$i]['tipo'].'">'.strtoupper($tipoActivo[$i]['subtipo']).'</option>';
                $comparaSubtipo = $switchSubtipo;
            }
        }
        ?>

    </div>
    <div class="row g-3" id="buscadorCampos">
        <div class="col">

                <select class="form-select" id="floatingSelectTres" aria-label="Floating label select example" name="selectTipo">
                    <option selected >Selecciona un tipo</option>
                    <?php echo $tipoSelect;?>
                    <option value="ver-todo">VER TODO</option>
                </select>


        </div>
        <div class="col">

                <select class="form-select" id="floatingSelectCuatro" aria-label=".form-select-lg example" name="selectSubtipo">
                    <option selected class="selecSubtipo">Selecciona un subtipo</option>
                    <?php echo $subtipoSelect;?>
                    <option value="ver-todo">VER TODO</option>
                </select>


        </div>
        <div class="col">

                <select class="form-select" id="floatingSelectUno" aria-label="Floating label select example" name="selectProvincia">
                    <option selected>Selecciona una provincia</option>
                    <?php echo $provinciaSelect;?>
                    <option value="ver-todo">VER TODO</option>
                </select>


        </div>
        <div class="col">

                <select class="form-select formLocalidades" id="floatingSelectDos" aria-label="Floating label select example">
                    <option selected class="selecLoc">Selecciona una localidad</option>
                    <?php echo $localidadSelect; ?>
                </select>


        </div>
        <div class="col">
            <button type="submit" class="btn btn-primary" id="btnBuscar">BUSCAR</button>
            <button type="submit" class="btn btn-primary" id="btnEliminar">QUITAR FILTROS</button>
        </div>
    </div>
</div>
<div class="container-xxl">
    <div class="row g-3">       
            <?php echo $contenedorCosas; ?>
        </div>

    <div class="row" id="infoNoEnecontrado">
        <div class="col-12">
            <h2 class="alert alert-danger m-3 mialerta" role="alert"> Lo sentimos pero no se han encontrado ningun activo para esos criterios de busqueda</h2>
        </h2>
    </div>
    </div>
</div>
<div class="totop"> 
            <a href="#top" class="btn btn-primary btnSubir">SUBIR <i class="fa-solid fa-angles-up"></i></a> 
        </div>
<footer class="footer bg-dark">
    <div class="container">
        <div class="row">
        <div class="col-6 col-md-3">
          <ul>
            <li style="margin-top:0 !important"><a href="https://www.mundofranquicia.com/contacta-con-mundofranquicia/contacta-con-mundofranquicia/">Contacto</a></li>
            <li style="margin-top:0 !important"><a href="https://www.mundofranquicia.com/consultora-de-franquicias/">Quiénes Somos</a></li>
            <li style="margin-top:0 !important"><a href="https://www.mundofranquicia.com/buscador-de-franquicias/">¿Buscas una franquicia?</a></li>
            <li style="margin-top:0 !important"><a href="https://www.mundofranquicia.com/buscador-de-franquicias/">¿Quieres franquiciar tu negocio?</a></li>
        </ul>
        </div>
    <div class="col-6 col-md-3">
        <ul>
            <li style="margin-top:0 !important"><a href="https://www.mundofranquicia.com/servifranquicia/">Servifranquicia</a></li>
            <li style="margin-top:0 !important"><a href="https://www.mundofranquicia.com/proyectos-de-franquicia/">Nuestros proyectos</a></li>
            <li style="margin-top:0 !important"><a href="https://www.mundofranquicia.com/noticias-sobre-franquicias/">Actualidad</a></li>
            <li style="margin-top:0 !important"><a href="https://www.mundofranquicia.com/eventos-sobre-franquicias/">Eventos</a></li>
        </ul>
    </div> 
    <div class="col-6 col-md-3">
    <ul>
 	<li style="margin-top:0 !important"><a href="https://www.mundofranquicia.com/politica-de-calidad/">Política de Calidad</a></li>
 	<li style="margin-top:0 !important"><a href="https://www.mundofranquicia.com/aviso-legal/">Aviso Legal</a></li>
 	<li style="margin-top:0 !important"><a href="https://www.mundofranquicia.com/wp-content/uploads/2016/07/Dossier-Guia-de-Franquicias-ST-1.pdf">Tarifas</a></li>
</ul>
    </div>
    <div class="col-6 col-md-3 text-end align-self-center">
                <div class="contRedes">
                    <a href="https://www.facebook.com/Mundofranquicia-Consulting-107778329266086/" target="_blank" class="ico-redes facebookIco"><i class="fa-brands fa-facebook fa-2x"></i></a>
                    <a href="https://www.facebook.com/Mundofranquicia-Consulting-107778329266086/" target="_blank" class="ico-redes twitterIco"><i class="fa-brands fa-twitter fa-2x"></i></a>
                    <a href="https://www.youtube.com/channel/UCG5zhkxY0ohstUmTduf6eXQ" target="_blank" class="ico-redes youtubeIco"><i class="fa-brands fa-youtube fa-2x"></i></a>
                    <a href="https://www.youtube.com/channel/UCG5zhkxY0ohstUmTduf6eXQ" target="_blank" class="ico-redes linkedinIco"><i class="fa-brands fa-linkedin fa-2x"></i></i></a>
                </div>
                <img src="/imagenes/logo-mundofranquicia-negativo.png" style="width:100%; max-width: 200px; margin-top: 10px;">
            </div>
        </div>
    </div>
    <div class="container-fluid pt-4" style="border-top: 1px solid grey">
<div class="row">
    <div class="col-12 text-center"><img src="/imagenes/logos-footer-miembros.png" style="max-width: 350px;"></div>
</div>
    </div>
   
</footer>
<?php 

function eliminar_acentos($cadena){
    //Reemplazamos la A y a
    $cadena = str_replace(
    array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
    array('a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'),
    $cadena
    );

    //Reemplazamos la E y e
    $cadena = str_replace(
    array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
    array('e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'),
    $cadena );

    //Reemplazamos la I y i
    $cadena = str_replace(
    array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
    array('i', 'i', 'i', 'i', 'i', 'i', 'i', 'i'),
    $cadena );

    //Reemplazamos la O y o
    $cadena = str_replace(
    array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
    array('o', 'o','o', 'o', 'o', 'o', 'o', 'o'),
    $cadena );

    //Reemplazamos la U y u
    $cadena = str_replace(
    array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
    array('u', 'u', 'u', 'u', 'u', 'u', 'u', 'u'),
    $cadena );

    //Reemplazamos la N, n, C y c
    $cadena = str_replace(
    array('Ñ', 'ñ', 'Ç', 'ç'),
    array('n', 'n', 'c', 'c'),
    $cadena
    );


    
    return $cadena;
}
?>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/gsap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/EaselPlugin.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/ScrollTrigger.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/EaselPlugin.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/MotionPathPlugin.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/ScrollToPlugin.min.js"></script>

<script src="js/principal.js"></script>
</body>
</html>
