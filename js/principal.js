$( document ).ready(function() {
    let varIdMay;

    let varTipo;
    let varSubtipo;
    let varProvincia;

    let varLocalidad;

    $('#floatingSelectUno').change( function(){
        varNombreProvincia = $('select[id=floatingSelectUno]').val();
        $('#floatingSelectDos option').each(function () {
            let varProvincia = $(this).attr("class");
            if(varProvincia != varNombreProvincia ) {
                $(this).css('display','none');
                $(this).attr("selected",false);
            } else {
                $(this).css('display','block');
                $('#floatingSelectDos .selecLoc').css('display','block');
                $('#floatingSelectDos .selecLoc').attr("selected",true);
            }
        });
    });
    $('#floatingSelectDos').change( function(){
        varSubtipo = $('select[id=floatingSelectDos]').val();
    });
    $('#floatingSelectCuatro').change( function(){
        varProvincia = $('select[id=floatingSelectCuatro]').val();
    });

    $('#floatingSelectTres').change( function(){
        varNombreTipo = $('select[id=floatingSelectTres]').val();
        $('#floatingSelectCuatro option').each(function () {
            let varTipo = $(this).attr("class");
            if(varTipo != varNombreTipo ) {
                $(this).css('display','none');
                $(this).attr("selected",false);
            } else {
                $(this).css('display','block');
                $('#floatingSelectCuatro .selecSubtipo').css('display','block');
                $('#floatingSelectCuatro .selecSubtipo').attr("selected",true);
            }
        });
    });


    let varArray = new Object();

    $('#btnBuscar').click(function(){
        varProvincia = $('select[id=floatingSelectUno]').val();
        varLocalidad = $('select[id=floatingSelectDos]').val();
        varTipo = $('select[id=floatingSelectTres]').val();
        varSubtipo = $('select[id=floatingSelectCuatro]').val();

        //varArray[varProvincia, varLocalidad, varTipo, varSubtipo];
        if(varProvincia=='Selecciona una provincia'){
            varArray.provincia = '';
        } else {
            varArray.provincia = varProvincia;
        }
        if(varLocalidad=='Selecciona una localidad'){
            varArray.localidad = '';
        } else {
            varArray.localidad = varLocalidad;
        }
        if(varTipo=='Selecciona un tipo'){
            varArray.tipo = '';
        } else {
            varArray.tipo = varTipo;
        }
        if(varSubtipo =='Selecciona un subtipo'){
            varArray.subtipo = '';
        } else {
            varArray.subtipo = varSubtipo;
        }
        console.log(varArray);

        $('.gridLocal').addClass('oculto');
        $('.gridLocal').each(function () {
            let switchTipo = $(this).attr('data-tipo');
            let switchSubtipo = $(this).attr('data-subtipo');
            let switchProvincia = $(this).attr('data-provincia');
            let switchLocalidad = $(this).attr('data-localidad');

            console.log(switchTipo);
            console.log(switchSubtipo);
            console.log(switchProvincia);
            console.log(switchLocalidad);


            if((switchTipo == varArray.tipo || varArray.tipo == '') && (switchSubtipo == varArray.subtipo || varArray.subtipo == '') && (switchProvincia == varArray.provincia || varArray.provincia == '') && (switchLocalidad == varArray.localidad || varArray.localidad == '')) {
                $(this).removeClass('oculto');
            } else {
                $('#infoNoEnecontrado').fadeIn();
            }
        });
    })
});
$('#btnEliminar').on('click',function(){
    $('#infoNoEnecontrado').fadeOut();
    $('.gridLocal').removeClass('oculto');
    $('select[id=floatingSelectUno]').prop('selectedIndex',0);
    $('select[id=floatingSelectDos]').prop('selectedIndex',0);
    $('select[id=floatingSelectTres]').prop('selectedIndex',0);
    $('select[id=floatingSelectCuatro]').prop('selectedIndex',0);

})

   $('.botonConsulta').on('click',function(){
       let miVar = $(this).data('ref');
       $('input#floatingRef').val(miVar);
       $('.capaFormulario').fadeIn();
   });

    $('.btnCerrar').on('click',function(){
        $('.capaFormulario').fadeOut();
    });

$('a.btnSubir').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 600);
    return false;
});

$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $('.totop a').fadeIn();
    } else {
        $('.totop a').fadeOut();
    }
});


    let descripciones = document.querySelectorAll('.contDescripcion');
    let varNombreLocal = document.querySelectorAll('.nombreLocal');

    let totalDivs = descripciones.length;
    let cargoVar = 0;
    let comparoVar = 0;
    let cargoNombre = 0;
    let comparoNombre = 0;
    for(let i=0; i<totalDivs; i++) {
        comparoVar = descripciones[i].offsetHeight;
        comparoNombre = varNombreLocal[i].offsetHeight;
        if(comparoVar > cargoVar) {
            cargoVar = comparoVar;
        }
        if(comparoNombre > cargoNombre) {
            cargoNombre = comparoNombre;
        }
    }

    $('.contDescripcion').css('height',cargoVar);


$( document ).ready(function() {
    $('#buscadorCampos').css('opacity',1);
    $('.capaLoader').fadeOut();

});
