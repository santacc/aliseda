<?php

            $data = file_get_contents("data/MundoFranquicia.json");
           // $data = file_get_contents("data/MundoFranquiciaNUEVO_NUEVO.json");
            $products = json_decode($data, true);



            $contenedorCosas = '';

            function limitar_cadena($cadena, $limite, $sufijo){
                // Si la longitud es mayor que el límite...
                if(strlen($cadena) > $limite){
                    // Entonces corta la cadena y ponle el sufijo
                    return substr($cadena, 0, $limite) . $sufijo;
                }

                // Si no, entonces devuelve la cadena normal
                return $cadena;
            }

            foreach ($products as $product) {
               $activos = $products['customerProperties'];
               //$activos = $products['inmuebles']['customerProperties'];
             }


            /*
                echo '<pre>';
                print_r($activos);
                echo '</pre>';


echo '<pre>';
        print_r(array_column($activos, 'type');
echo '</pre>';
        array_multisort(array_column($activos, 'type'), SORT_DESC, SORT_STRING );
            */
            // print_r($activosDeve);

             $contActivo = 0;
           
             $provLocalidad = array();
             $tipoActivo = array();
             $keyTipo = 0;
             $keyprovincia = 0;



function cmp($a, $b)
{
    return strcmp($a["type"], $b["type"]);
}

    usort($activos,"cmp");
    foreach ($activos as $activo) {
                $contenedorDatos = '';
                $contenedorDecripcion = '';
                $provLocalidad[$keyprovincia]['provincia'] = $activo["propertyAddress"]["addressProvince"];
                $provLocalidad[$keyprovincia]['localidad']= $activo["propertyAddress"]["addressTown"];
                $tipoActivo[$keyTipo]['tipo'] = $activo["type"];
                $tipoActivo[$keyTipo]['subtipo'] = $activo["subType"];

                $descripcionLocal = $activo["propertyDescriptions"];


                for($j=0; $j<count($descripcionLocal); $j++) {
                    $contenedorDecripcion .= '<div class="contDescripcion">'. limitar_cadena($descripcionLocal[$j]["descriptionText"],300,'...').'</div>';
                }
                $imagenesActivo = $activo["propertyImages"];
                $contenedorFotos = '';
                $indicadoresFotos = '';
                $contImagenes = '';
                $indicadoresFotos ='<div class="carousel-indicators">';

                $varContImagenes = count($imagenesActivo);
                if($varContImagenes >= 5) {
                    $contadorImagenes = 5;
                } else {
                    $contadorImagenes = count($imagenesActivo);
                }

           for ($i=0; $i<$contadorImagenes; $i++) {
                    if($i==0) {
                        $contImagenes .= '<div class="carousel-item active"><img src="'.$imagenesActivo[$i]['imageUrl'].'" width="100%"></div>';
                       // $indicadoresFotos .= '<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="'.$i.'" class="active" aria-current="true" aria-label="Slide '.$i.'"></button>';
                    } else {
                        $contImagenes .= '<div class="carousel-item"><img src="'.$imagenesActivo[$i]['imageUrl'].'" width="100%"></div>';
                      //  $indicadoresFotos .='<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="'.$i.'" aria-label="Slide '.$i.'"></button>';
                    }  
               }

               $indicadoresFotos    .= '</div>';
               $contenedorFotos     .= '<div id="carouselExampleIndicators'.$contActivo.'" class="carousel slide" data-bs-ride="carousel" data-bs-interval="0">';
              // $contenedorFotos     .= $indicadoresFotos;
               $contenedorFotos     .= '<div class="carousel-inner">'.$contImagenes.'</div>
                                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators'.$contActivo.'" data-bs-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Previous</span>
                                        </button>
                                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators'.$contActivo.'" data-bs-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Next</span>
                                        </button>';
                $contenedorFotos    .= '</div>';
            
                $codigoLocal        = $activo['propertyCode'];
                $calleLocal         = $activo["propertyAddress"]["addressStreetName"];
                $numeroCalle        = $activo["propertyAddress"]["addressStreetNumber"];
                $plantaLocal        = $activo["propertyAddress"]["addressFloor"];
                $codigoPostalLocal  = $activo["propertyAddress"]["addressPostalCode"];
                $ciudadLocal        = $activo["propertyAddress"]["addressTown"];
                $ciudadLocalTrans   = eliminar_acentos(str_replace(' ','-',strtolower($ciudadLocal)));
                $provinciaLocal     = $activo["propertyAddress"]["addressProvince"];
                $provinciaLocalMin  = eliminar_acentos(strtolower($provinciaLocal));
                $paisLocal          = $activo["propertyAddress"]["addressCountry"];
                $metrosLocal        = $activo["areaConstructed"];
                $tipoLocal          = $activo["type"];
                $subtipoLocal       = $activo["subType"];

                
                $contenedorDatos .= '<div class="contDatos p-3">';
                
                $contenedorDatos .= '<div class="nombreLocal">'.$calleLocal.'</div>';
                $contenedorDatos .= '<div class="tipoLocal">'.$tipoLocal .' / '.$subtipoLocal .'</div>';
                $contenedorDatos .= '<hr>'; 
                $contenedorDatos .=  '<div class="contDescripcion">'.strip_tags($contenedorDecripcion).'</div>';
                $contenedorDatos .= '<hr>'; 
				$contenedorDatos .= '<div class="textDatos"><div class="etiquetaCont">Localidad: </div><div class="descripcionCont">'.$ciudadLocal.'</div></div>';
                $contenedorDatos .= '<div class="textDatos"><div class="etiquetaCont">C.P: </div><div class="descripcionCont">'.$codigoPostalLocal.'</div></div>';
                $contenedorDatos .= '<div class="textDatos"><div class="etiquetaCont">Provincia: </div><div class="descripcionCont">'.$provinciaLocal.'</div></div>';
				//$contenedorDatos .= '<div class="textDatos"><div class="etiquetaCont">País: </div><div class="descripcionCont">'.$paisLocal.'</div></div>';
                $contenedorDatos .= '<hr>'; 
                $contenedorDatos .= '<div class="textDatosCol"><div class="etiquetaContCol">Metros<sup>2</sup>:</div><div class="descripcionContCol">'.$metrosLocal.'</div></div>';         
                $contenedorDatos .= '<div class="textDatosCol"><div class="etiquetaContCol">Número: </div><div class="descripcionContCol">'.$numeroCalle.'</div></div>';
				$contenedorDatos .= '<div class="textDatosCol"><div class="etiquetaContCol">Planta: </div><div class="descripcionContCol">'.$plantaLocal.'</div></div>';
                $contenedorDatos .= '<hr>'; 
                $contenedorDatos .= '<div class="codigoLocal">REF: '. $codigoLocal .'</div>';
				$contenedorDatos .= '</div>';
        
                
                $contenedorCosas .= '<div id="'.$activo['propertyCode'].'" class="col-12 col-md-6 col-lg-4 col-xl-4 gridLocal" data-provincia="'.$provinciaLocalMin.'" data-localidad="'.$ciudadLocalTrans.'" data-tipo="'.$tipoLocal.'" data-subtipo="'.$subtipoLocal .'" ><div class="p-0 border bg-light">';
                if($contImagenes != '') {
                    $contenedorCosas .= $contenedorFotos;
                } else {
                    $contenedorCosas .= '<img src="/imagenes/falta_imagen.jpg" width="100%">';
                }

                
                $contenedorCosas .= $contenedorDatos;		    
                $contenedorCosas .= '<div class="d-grid gap-2 mx-auto"><div class="btn btn-primary btn-principal botonConsulta" data-ref="'. $codigoLocal .'" type="button" role="alert" >CONSULTAR PRECIO</div>';
                $contenedorCosas .= '</div></div></div>';
                // echo '<pre>';
                // var_dump($activo);
                // echo '</pre>';
                $contActivo++;
                $keyprovincia++;
                 $keyTipo++;

             }

             
             ?>