# JSON ALISEDA

## Campos para los activos

propertyCode: "CAN0000175347",
propertyRentingPrize : 0,
propertyAddress : {
    addressVisibility : "street",
    addressStreetName : "SECT 89/3 ARCOSUR SE.C5-2",
    addressStreetNumber : 0.0,
    addressFloor : null,
    addressPostalCode : "50011",
    addressTown : "ZARAGOZA",
    addressProvince : "Zaragoza",
    addressCountry : "Spain",
    addressCoordinatesPrecision : "exact",
    addressCoordinatesLatitude : 41.6287422654844,
    addressCoordinatesLongitude : -0.960576637973784
            },
            propertyDescriptions : [
                {
                    descriptionLanguage": "spanish",
                    descriptionText": "Suelo en venta ubicado en SECT 89/3 ARCOSUR SE.C5-2, ZARAGOZA, Zaragoza. Dispone de una superficie de 2550 m2  "
                }
            ],
            areaConstructed : null,
            areaPlot : null,
            propertyImages : [
                {
                    imageUrl : "https://storage.googleapis.com/aliseda/ImagenesActivos/Espana/Aragon/Zaragoza/CAN0000175347/CAN0000175347-A.jpg"
                },
                {
                    imageUrl : "https://storage.googleapis.com/aliseda/ImagenesActivos/Espana/Aragon/Zaragoza/CAN0000175347/CAN0000175347-B.jpg"
                },
                {
                    imageUrl : "https://storage.googleapis.com/aliseda/ImagenesActivos/Espana/Aragon/Zaragoza/CAN0000175347/CAN0000175347-C.jpg"
                },
            
            ]
